var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');
var stripe = require("stripe")("sk_test_SZTECCqFQ9NHVOI5p5uWwa6C");

// configure app to use bodyParser() this will let us get the data from a POST
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
  res.render(path.join(__dirname, 'public', 'index.ejs'));
});

app.get('/donate', function (req, res) {
  res.render(path.join(__dirname, 'public', 'donate.ejs'));
});

// Token is created using Stripe.js or Checkout! Get the payment token submitted
// by the form:
app.post('/charge', function (req, res) {

  var token = req.body.stripeToken;
  var chargeAmount = req.body.chargeAmount;

  var name = req.body.fullname;
  var email = req.body.email;
  var address = req.body.address;
  var phone = req.body.phone;
  var occupation = req.body.occupation;

  var charge = stripe
    .charges
    .create({
      amount: chargeAmount * 100,
      currency: "usd",
      source: token,
      description: "Diego Morales Donation",
      receipt_email: email,

      metadata: {
        'name': name,
        'address': address,
        'phone': phone,
        'email': email,
        'occupation': occupation
      }
    }, function (err, charge) {
      if (err === "StripCardError") {
        console.log("Your card was declinded");
      }
    });
  console.log("Payment was a success")
  res.redirect('/');
});

var server = app.listen(process.env.PORT || 8080, function () {
  var host = server
    .address()
    .address
  var port = server
    .address()
    .port

  console.log("Example app listening at http://%s:%s", host, port)
})